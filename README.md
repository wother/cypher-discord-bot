# Cypher Dice Bot #

A Discord Bot for the Cypher table top RPG.

## Installation ##

`npm i` or `npm install` will grab and install all the dependancies you need

## Running The Bot ##

run `node bot.js`

## Attribution ##

base code from the discord-greter-bot on github
[link](http.url.com)